#include <QApplication>
#include <QHotkey/QHotkey/qhotkey.h>
#include <ui_ragnaryk.h>
#include <ragnaryk.h>

int main(int argc, char *argv[]) {
  QApplication::setStyle("motif");
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
  QCoreApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);

  QApplication application(argc, argv);

  QWebEngineSettings::defaultSettings()->setAttribute(QWebEngineSettings::PluginsEnabled, true);

  application.setApplicationName(QString("Ragnaryk"));
  application.setApplicationVersion(QString("1.0"));

  Ragnaryk *mainwindow = new Ragnaryk();

  mainwindow->show();

  QHotkey refresh_hotkey(QKeySequence("f5"), true);//The hotkey will be automatically registered

  QObject::connect(&refresh_hotkey, &QHotkey::activated, qApp, [&](){
          mainwindow->ui->browser->reload();
  });

  return application.exec();
}
