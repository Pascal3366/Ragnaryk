#ifndef RAGNARYK_H
#define RAGNARYK_H

#include <QMainWindow>
#include <QtWebEngine/QtWebEngine>
#include <QtWebEngineWidgets/QtWebEngineWidgets>
#include <QtWebEngineWidgets/QWebEngineView>
#include <QtWebEngineCore/QtWebEngineCore>
#include <QtAwesome/QtAwesome.h>
#include <QPushButton>
#include <QIcon>

namespace Ui {
  class Ragnaryk;
}

class Ragnaryk : public QMainWindow
{
  Q_OBJECT

public:
  explicit Ragnaryk(QWidget *parent = 0);
   Ui::Ragnaryk *ui;
  ~Ragnaryk();

  private slots:
    void on_go_btn_clicked();

    void on_back_btn_clicked();

    void on_forward_btn_clicked();

    void on_refresh_btn_clicked();

    void on_browser_loadProgress(int progress);

    void on_actionAbout_Ragnaryk_triggered();

    void on_browser_urlChanged(const QUrl &arg1);

    void on_url_edit_returnPressed();

    void on_browser_loadFinished(bool arg1);

    void on_browser_loadStarted();

    void acceptFullScreenRequest(QWebEngineFullScreenRequest request);

    void fullScreenRequested(QWebEngineFullScreenRequest request);

    //void on_actionNewTab_triggered();

    void on_tabWidget_currentChanged(int index);

private:
    QWebEngineView *browser;
    QString homepage = "https://www.duckduckgo.com";
    void refresh();
    void openInNewTab();
    void newTab();
};

#endif // RAGNARYK_H
